//
//  ViewController.h
//  AdditionTwoNumbers
//
//  Created by Jayesh Mathur on 29/11/16.
//  Copyright © 2016 Jayesh Mathur. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController
@property (weak, nonatomic) IBOutlet UITextField *firstNumber;
@property (weak, nonatomic) IBOutlet UITextField *secondNumber;
@property (weak, nonatomic) IBOutlet UILabel *resultLabel;
@property (weak, nonatomic) IBOutlet UIButton *addButton;

@end

