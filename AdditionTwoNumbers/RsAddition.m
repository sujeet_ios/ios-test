//
//  RsAddition.m
//  AdditionTwoNumbers
//
//  Created by Jayesh Mathur on 29/11/16.
//  Copyright © 2016 Jayesh Mathur. All rights reserved.
//

#import "RsAddition.h"

@implementation RsAddition
-(NSInteger)addNumberOne:(NSInteger) firstNumber withNumberTwo:(NSInteger) secondNumber
{
    return firstNumber + secondNumber;
}
@end
