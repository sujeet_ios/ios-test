//
//  RsAddition.h
//  AdditionTwoNumbers
//
//  Created by Jayesh Mathur on 29/11/16.
//  Copyright © 2016 Jayesh Mathur. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface RsAddition : NSObject
-(NSInteger)addNumberOne:(NSInteger) firstNumber withNumberTwo:(NSInteger) secondNumber;
@end
