//
//  AppDelegate.h
//  AdditionTwoNumbers
//
//  Created by Jayesh Mathur on 29/11/16.
//  Copyright © 2016 Jayesh Mathur. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

