//
//  AddTwoNumberViewControllerTests.m
//  AddingTwoNumbers
//
//  Created by Ravi Shankar on 14/04/14.
//  Copyright (c) 2014 Ravi Shankar. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "ViewController.h"

@interface AddTwoNumberViewControllerTests : XCTestCase

@end

@implementation AddTwoNumberViewControllerTests
{
    ViewController *viewController;
}

- (void)setUp
{
    [super setUp];
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    viewController = [storyboard instantiateViewControllerWithIdentifier:
                      @"RSViewController"];
    [viewController view];
}

- (void)tearDown
{
    [super tearDown];
}

-(void)testViewControllerViewExists {
    XCTAssertNotNil([viewController view], @"ViewController should contain a view");
}

-(void)testFirstNumberTextFieldConnection {
    XCTAssertNotNil([viewController firstNumber], @"firstNumberTextField should be connected");
}

-(void)testSecondNumberTextFieldConnection {
    XCTAssertNotNil([viewController secondNumber], @"secondNumberTextField should be connected");
}

-(void)testresultTextFieldConnection {
    XCTAssertNotNil([viewController resultLabel], @"resultTextField should be connected");
}

-(void)testAddButtonConnection {
    XCTAssertNotNil([viewController addButton], @"add button should be connected");
}

-(void)testAddButtonCheckIBAction {
    
    NSArray *actions = [viewController.addButton actionsForTarget:viewController
                                                  forControlEvent:UIControlEventTouchUpInside];
    XCTAssertTrue([actions containsObject:@"addNumbers:"], @"");
}

-(void)testAddingTenPlusTwentyShouldBeThirty {
    viewController.firstNumber.text = @"10";
    viewController.secondNumber.text = @"20";
    [viewController.addButton sendActionsForControlEvents: UIControlEventTouchUpInside];
    XCTAssertEqualObjects(viewController.resultLabel.text,@"30","Result of the textfield should be 30");
}





@end
