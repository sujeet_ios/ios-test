//
//  AdditionTwoNumbersTests.m
//  AdditionTwoNumbersTests
//
//  Created by Jayesh Mathur on 29/11/16.
//  Copyright © 2016 Jayesh Mathur. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "RsAddition.h"
@interface AdditionTwoNumbersTests : XCTestCase

@end

@implementation AdditionTwoNumbersTests
{

    RsAddition *addition;
}
- (void)setUp {
    [super setUp];
    addition = [[RsAddition alloc]init];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

-(void)testAdditionClassExists
{  
    
    XCTAssertNotNil(addition,@"RSAddition class exists");
 

}
-(void)testAddTwoPlusTwo
{

    NSInteger result = [addition addNumberOne:2 withNumberTwo:2];
    XCTAssertEqual(result, 4,@"Addition of 2 + 2 is 4");

}

-(void)testAddTwoPlusSeven {
    NSInteger result = [addition addNumberOne:2 withNumberTwo:7];
    XCTAssertEqual(result, 9,@"Addition of 2 + 7 is 9");
}
- (void)testPerformanceExample {
    // This is an example of a performance test case.
    [self measureBlock:^{
        // Put the code you want to measure the time of here.
    }];
}

@end
